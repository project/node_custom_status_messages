<?php

namespace Drupal\node_custom_status_messages\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class NodeCustomMessageSettingsForm.
 */
class NodeCustomMessageSettingsForm extends ConfigFormBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'node_custom_status_messages.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'node_custom_status_messages_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('node_custom_status_messages.settings');
    //kint($config);
    $form['node_custom_status_messages_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Node Custom Status Message'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
    ];

    $node_type = $this->entityTypeManager->getStorage('node_type');

    $options = array_map(function ($node_type) {
      return $node_type->label();
    }, $node_type->loadMultiple()
    );

    foreach($options as $options_key => $options_value){
      $default_value_check = $config->get($options_key) ?? [];
      $default_update_status = $config->get('update_status'.$options_key) ?? [];
      $default_add_status = $config->get('add_status'.$options_key) ?? [];
      $default_delete_status = $config->get('delete_status'.$options_key) ?? [];
      $form['node_custom_status_messages_settings'][$options_key] = [
        '#type' => 'checkboxes',
        '#options' => array($options_key => $options_value ),
        '#default_value' => $default_value_check,
      ];
      $form['node_custom_status_messages_settings']['node_status_box'.$options_key] = [
        '#type' => 'container',
        '#states' => array(
          'invisible' => array(
            'input[name="'.$options_key.'['.$options_key.']"]' => array(
              'checked' => FALSE,
            ),
          ),
        ),
      ];
      $form['node_custom_status_messages_settings']['node_status_box'.$options_key]['add_status'.$options_key] = [
        '#type' => 'textfield',
        '#title' => $this->t('Create Status Message('.$options_value.')'),
        '#description'   => $this->t('Custom status message when user create the node.'),
        '#default_value' => $default_add_status,
      ];
      $form['node_custom_status_messages_settings']['node_status_box'.$options_key]['update_status'.$options_key] = [
        '#type' => 'textfield',
        '#title' => $this->t('Update Status Message('.$options_value.')'),
        '#description'   => $this->t('Custom status message when user updated the node.'),
        '#default_value' => $default_update_status,
      ];
      $form['node_custom_status_messages_settings']['node_status_box'.$options_key]['delete_status'.$options_key] = [
        '#type' => 'textfield',
        '#title' => $this->t('Delete Status Message('.$options_value.')'),
        '#description'   => $this->t('Custom status message when user delete the node.'),
        '#default_value' => $default_delete_status,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $node_custom_status_messages_config = $this->config('node_custom_status_messages.settings');
    theme_settings_convert_to_config($values, $node_custom_status_messages_config)->save();
  }

}
