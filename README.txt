INTRODUCTION
------------

This module gives site owner options to specify custom messages shown to end users.
The core drupal message system as offered by drupal_set_message is an excellent way for modules to send out messages to the end users.
However not all drupal site owners are keen to show all the messages sent out by drupal core and all modules to their users.
This module gives site administrators a reasonably powerful way to apply custom messages shown to the end users in case of node add/update/delete.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/extending-drupal/installing-drupal-modules for further information.

CONFIGURATION
-------------

Once you have enable the module, open the configuration setting page from
/admin/config/user-interface/node_custom_messages_settings. Apply your custom
messages with respect to each node and save the configuration.

MAINTAINERS
-----------

Current maintainers:
 * Mahtab Alam (mahtabalam) - https://www.drupal.org/u/mahtabalam

This project has been sponsored by:
 * Hureka Technologies Inc. (https://www.drupal.org/hureka-technologies-inc)
